package org.dxc.entity;

public class Employee {
	public Employee(String fName, String lName, int salary) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	private int id;
	private String fName;
	private String lName;
	private int salary;

	public Employee() {
	}

}
