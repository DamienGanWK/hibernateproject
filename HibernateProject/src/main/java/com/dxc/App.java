package com.dxc;

import java.util.Iterator;
import java.util.List;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.SystemException;

import org.dxc.entity.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.Transaction;

class App {
	private static SessionFactory factory;

	public static void main(String[] args)
			throws SecurityException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		try {
			// factory obj created
			factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable e) {
			System.err.println("Failed to create Session");
			throw new ExceptionInInitializerError(e);
		}

		App employee = new App();

		// Add employee details
		Integer empID1 = employee.addEmployee("Damien", "Gan", 9000);
		Integer empID2 = employee.addEmployee("John", "Doe", 10000);
		Integer empID3 = employee.addEmployee("Jack", "Sparrow", 8000);

		/* Method to CREATE an employee in the database */
		employee.listEmployees();

		/* Update employee's records */
		employee.updateEmployee(3, 5000);

		/* Delete an employee from the database */
		employee.deleteEmployee(2);

		/* List down new list of the employees */
		employee.listEmployees();

	}

	/* Method to CREATE an employee in the database */
	public Integer addEmployee(String fname, String lname, int salary)
			throws SecurityException, HeuristicMixedException, HeuristicRollbackException, SystemException {
		// session obj to be extracted from factory
		Session session = factory.openSession();
		Transaction tx = null;
		Integer employeeID = null;

		try {
			tx = session.beginTransaction();
			// object model is ready
			Employee employee = new Employee(fname, lname, salary);
			// add it to relational model
			employeeID = (Integer) session.save(employee);
			tx.commit();
			System.out.println("Employee Record Created");

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				e.printStackTrace();
			}
		} finally {
			session.close();
		}
		return employeeID;

	}

	/* Method to READ all the employees */
	public void listEmployees() {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List employees = session.createQuery("FROM Employee").list();
			for (Iterator iterator = employees.iterator(); iterator.hasNext();) {
				Employee employee = (Employee) iterator.next();
				System.out.print("First Name: " + employee.getfName());
				System.out.print("  Last Name: " + employee.getlName());
				System.out.println("  Salary: " + employee.getSalary());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to UPDATE salary for an employee */
	public void updateEmployee(Integer EmployeeID, int salary) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class, EmployeeID);
			employee.setSalary(salary);
			session.update(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to DELETE an employee from the records */
	public void deleteEmployee(Integer EmployeeID) {
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Employee employee = (Employee) session.get(Employee.class, EmployeeID);
			session.delete(employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
